# UI Shaker

A simple UI shaker for Castle Game Engine

# License

License is the same as Castle Game Engine, unless explicitly specified, see https://castle-engine.io/license.php
